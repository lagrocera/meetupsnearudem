//
//  main.m
//  MeetUpsNearUdeM
//
//  Created by Luis F Ruiz Arroyave on 11/1/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
